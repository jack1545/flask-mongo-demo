# coding:utf-8

from flask import Flask,render_template
from flask_pymongo import PyMongo
from flask_admin import Admin
from flask_admin.contrib.pymongo import ModelView
from wtforms import form,fields
from flask_ckeditor import CKEditor,CKEditorField

app = Flask(__name__)
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
app.secret_key = '13456'
mongo = PyMongo(app, uri='mongodb://localhost:27017/Score')
ckeditor = CKEditor(app)

class ArticleForm(form.Form):
    name = fields.StringField('Name')
    url = fields.StringField('Url')
    country = fields.StringField('Country')
    birth = fields.StringField('Birth')
    histroy = CKEditorField('Histroy')

class ArticleView(ModelView):
    column_list = ('name','url','country', 'birth','histroy')
    column_searchable_list = ('name', 'country')

    create_template = 'edit.html'
    edit_template = 'edit.html'

    form = ArticleForm

admin = Admin(app,name='microblog',template_mode='bootstrap3')
admin.add_view(ArticleView(mongo.db['Player_LDL']))

@app.route('/')
def index():
    data = mongo.db.Player_LDL.find({}).limit(10)
    return render_template('index.html',data=data)

if __name__ == '__main__':
    app.run()