# coing:utf-8

def pages(sum, db, cname, sortby=''):
    collection = db[cname].find()

    if sortby:
        '''自定义排序规则'''
        data = collection.sort([sortby]).limit(sum)

    data = collection.sort([('datetime',-1)]).limit(sum)
    collection_count = collection.count()
    page_sum = int(collection_count / sum)

    return data, page_sum