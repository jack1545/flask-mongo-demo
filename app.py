from flask import Flask,render_template,request
import pymongo,re
from flask_bootstrap import Bootstrap
from tools import pages

app = Flask(__name__)
bootstrap = Bootstrap(app)

mdb = pymongo.MongoClient('localhost',27017)['Score_Games']
mdb2 = pymongo.MongoClient('localhost',27017)['Score']

@app.route('/')
def index():
    routes = ['/esport-news/','/player-lol/','/player-ldl/',
              '/player-outside/','/player-outside/','/player-csgo/',
              '/team-csgo/','/team-csgo5e/','/transfer2020/',
              '/en-cosplay/','/girl-list/'
              ]

    return render_template('index.html',routes=routes)

@app.route('/esport-news/')
def news():
    esport_news = mdb.db.Score_Commit.find({'pass': 1, }).sort([('_id', -1)])
    esport_news_for_wanplus = mdb.db.Wanplus_Commit.find({'pass': 1, }).sort([('_id', -1)])
    return render_template('index.html',news_list=esport_news,news_list_wp=esport_news_for_wanplus)

@app.route('/player-lol/')
def player():
    players_data = mdb2['Player_Liquipedia'].find({})
    return render_template('player.html',player_data=players_data)

@app.route('/player-lol/<name>/')
def player_detail(name):
    player_data = mdb2['Player_Liquipedia'].find({'name':name})
    return render_template('player_detail.html',player_data=player_data)

@app.route('/player-ldl/')
def player_ldl():
    player_data = mdb2.db.Player_LDL.find({})
    return render_template('player_ldl.html',player_data=player_data)

@app.route('/player-outside/')
def player_outside():
    player_data = mdb2.db.Player_Gamepedia.find({})
    return render_template('player_outside.html',player_data=player_data)

@app.route('/player-csgo/')
def player_csgo():
    player_data = mdb2.db.CSGO_Player_Liquipedia.find({})
    if player_data:
        return render_template('player_csgo.html',player_data=player_data)

@app.route('/team-csgo/')
def team_csgo():
    data = mdb2.db.CSGO_Team_Liquipedia.find({})
    return render_template('team_csgo.html',data=data)

@app.route('/team-csgo5e/')
def team_csgo5e():
    data = mdb2.db.CSGO_Team_5eplay.find({})
    return render_template('team_csgo5e.html',data=data)

@app.route('/transfer2020/')
def transfer2020():
    data = mdb2.db.Transfer2020.find({})
    return render_template('transfer2020.html',data=data)

@app.route('/en-cosplay/')
def en_cosplay():
    data = mdb2.db.EN_Cosplay.find({})
    return render_template('en_cosplay.html',data=data)

@app.route('/girl-<int:id>/')
def pic_girl_pconline(id):
    data = mdb2['Soccer_Pconline'].find_one({'id':str(id)})
    return render_template('pic_simple_pconline.html',data=data)

@app.route('/girl-pconline/')
def pic_girl_list_pconline():
    # page_size = 30
    # page_no = request.args.get('page')
    # skip = page_size * (page_no - 1) if page_no else 0
    # data = mdb2['Soccer_Girls'].find({}).limit(page_size).skip(skip)
    data = mdb2['Soccer_Pconline'].find({'status':'exist'})
    return render_template('pic_list_pconline.html',data=data)

@app.route('/match-<string:id>/')
def pic_match_simple(id):
    data = mdb2['Soccer_Sina'].find_one({'id':str(id)})
    return render_template('pic_simple_sina.html',data=data)

@app.route('/match-sina/')
def pic_match():
    name = u'新浪图库'
    data,sum  = pages(10,mdb2,'Soccer_Sina')
    return render_template('pic_list_sina.html',data=data,sum=sum)

@app.route('/match-sina-<int:id>/')
def pic_match_pages(id):
    data,sum = pages(10,mdb2,'Soccer_Sina')
    data = data.skip((id - 1) * 10)
    return render_template('pic_list_sina.html',data=data,sum=sum,id=id)

'''第一图库足球宝贝图片'''
# @app.route('/girl-<int:id>/')
# def pic_girl(id):
#     data = mdb2['Soccer_Girls'].find_one({'id':str(id)})
#     return render_template('girlpic.html',data=data)
#
# @app.route('/girl-list/')
# def pic_girl_list():
#     # page_size = 30
#     # page_no = request.args.get('page')
#     # skip = page_size * (page_no - 1) if page_no else 0
#     # data = mdb2['Soccer_Girls'].find({}).limit(page_size).skip(skip)
#     data = mdb2['Soccer_Girls'].find({})
#     return render_template('girlpic_list.html',data=data)

@app.route('/girl-list/<int:listid>/')
def pic_list_page(listid):
    data = mdb2['Soccer_Girls'].find({}).limit(30).skip(listid)
    return render_template('girlpic_list.html',data=data)

@app.route('/search')
def search():
    q = request.args.get('q').strip()
    data = mdb2['Soccer_Girls'].find({'title':{'$regex':'{0}'.format(q)}})
    return render_template('search.html',q=q,data=data)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404