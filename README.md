# 2021.04.21 flask-admin + mongodb + CKEditor 制作简单的 mongodb 管理后台

参考：
1. Flask-Admin集成CKEditor：https://zhuanlan.zhihu.com/p/57379829
2. Flask-Admin利用动态生成类访问Mongo：https://blog.csdn.net/zjh776/article/details/88973793
3. Flask-ADmin官方示例链接Pymongo：https://github.com/flask-admin/flask-admin/tree/master/examples/pymongo