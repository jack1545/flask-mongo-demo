@app.route('/')  # 定义路由(Views)，可以理解为定义页面的URL
def index():
    getsrc = request.args.get('src')
    p = request.args.get('p')
    if getsrc == None:
        src = 'xxxxx'
    else:
        src = getsrc
    show_status = 0
    if not p:
        p = 1
    else:
        p = int(p)
        if p > 1:
            show_status = 1

    limit_start = (p - 1) * 10

    result = db.fiancenews.find({'src': src}).sort([("published", -1)]).limit(10).skip(limit_start)

    total = db.fiancenews.find({'src': src}).count()

    page_total = int(math.ceil(total / 10))
    page_list = get_page(page_total, p)
    datas = {
        'data_list': result,
        'src': src,
        'p': p,
        'page_total': page_total,
        'show_status': show_status,
        'page_list': page_list
    }
    return render_template('index.html', datas=datas)